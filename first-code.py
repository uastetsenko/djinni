import requests
from bs4 import BeautifulSoup

full_data = []  # Создаю список для хранения конечных результатов


def get_html(url):
    data = {
        'TIP_Consulta': '5',
        'TIP_Lengueta': 'tdDos',
        'FEC_Desde': '01/02/2019',  # Дата начала
        'FEC_Hasta': '02/02/2019',  # Дата конца
        'irAccionAtPublico': 'Consulta',
        'COD_Tribunal': '1336',  # Соответствует - Juzgado de Letras del Trabajo de Copiapo
        'COD_TribunalSinTodos': '1336'  # Соответствует - Juzgado de Letras del Trabajo de Copiapo
    }  # Устанавливаем фильтры

    headers = {
        'Cookie': 'JSESSIONID=0000TKgweM8WJ8cfdXmarq3s4as:-1'
    }  # Задаем куки (без них не работает)

    try:
        response = requests.post(url, data=data, headers=headers, timeout=10)
        if response.status_code == 404:  # Проверяем на 404 ошибку
            return 'Error: 404'
        else:
            return response.text
    except requests.exceptions.ConnectionError:  # Проверяем на ошибку ConnectionError
        return 'Error: ConnectionError'
    except requests.exceptions.MissingSchema:  # Проверяем на ошибку MissingSchema
        return 'Error: MissingSchema'
    except requests.exceptions.ReadTimeout:  # Проверяем на ошибку ReadTimeout
        return 'Error: ReadTimeout'
    except requests.exceptions.TooManyRedirects:  # Проверяем на ошибку TooManyRedirects
        return 'Error: TooManyRedirects'


def get_data():
    i = 0  # Устанавливаю счетчик для определения первой строки
    url = 'https://laboral.pjud.cl/SITLAPORWEB/AtPublicoDAction.do'  # Указываю url
    html = get_html(url)  # Получаю html код из POST запроса
    soup = BeautifulSoup(html, 'lxml')  # Перевожу в красивый вид bs4

    if 'Error' not in html:  # Проверяю нет ли ошибок
        try:
            main_div = soup.find('div', id='divContenedor2').find_all('tr')  # Получаю все таблицу и строки

            for all_td in main_div:  # Получаю все строки
                if i == 0:
                    i += 1
                    continue  # Пропускаю первый элемент (он всегда пустой)

                raw_data = {}  # Создаю словарь для хранения данных

                try:
                    raw_text = all_td.find_all('td')  # Получаю все TD
                    try:
                        raw_data['RUC'] = raw_text[1].text  # Получаю RUC
                        raw_data['Date'] = raw_text[2].text  # Получаю Дату
                        raw_data['Name'] = raw_text[3].text  # Получаю Название
                        raw_data['Court'] = raw_text[4].text  # Получаю Суд
                    except IndexError:
                        raw_data = 'Error: Bad one of index'  # Если один из TD не получается
                except IndexError:
                    raw_data = 'Error: Not Enough TD'  # Если нет ни единого TD

                full_data.append(raw_data)  # Записываю все в словарь
        except AttributeError:
            full_data.append('AttributeError')  # Если не могу получить главную таблицу с данными
    else:
        full_data.append(html)  # Записываю ошибку в конечный словарь


if __name__ == '__main__':
    get_data()  # Запускаю основной код

    print(full_data)  # Вывожу все данные

