#!/bin/bash

# ---------------------------------
# Configure Network
# ---------------------------------

# PATH to interfaces
FN=/etc/network/interfaces

echo "" >> $FN
echo "iface eth0 inet static" >> $FN

echo "Please write IP"
while true; do
    read -p "-> Set (10.0.0.2)? (Y/n) " yn
    case $yn in
<------>[Yy]* ) echo "address 10.0.0.2" >> $FN; break;;
<------>[Nn]* ) read -p "Please write IP manually: " from_user_ip;.
<------>    echo "address $from_user_ip" >> $FN; break;;
<------>* ) echo "Please answer yes or no.";;.
    esac
done

echo "Please write Netmask"
while true; do
    read -p "-> Set (255.255.0.0)? (Y/n) " yn
    case $yn in
<------>[Yy]* ) echo "netmask 255.255.0.0" >> $FN; break;;
<------>[Nn]* ) read -p "Please write Netmask manually: " from_user_netmask;.
<------>    echo "netmask $from_user_netmask" >> $FN; break;;
<------>* ) echo "Please answer Yes or No.";;
    esac
done

echo "Please write Gateway"
while true; do
    read -p "-> Set (10.0.0.1)? (Y/n) " yn
    case $yn in
<------>[Yy]* ) echo "gateway 10.0.0.1" >> $FN; break;;
<------>[Nn]* ) read -p "Please write Gateway manually: " from_user_gateway;
<------>    echo "gateway $from_user_gateway" >> $FN; break;;
<------>* ) echo "Please answer Yes or No.";;
    esac
done

echo "Please write DNS"
while true; do
    read -p "-> Set (8.8.8.8 and 8.8.4.4)? (Y/n) " yn
    case $yn in
<------>[Yy]* ) echo "dns-nameservers 8.8.8.8 8.8.4.4" >> $FN; break;;
<------>[Nn]* ) echo "Please write DNS manually";.
<------>read -p "First: " from_user_fd;
<------>read -p "Second: " from_user_sd;
<------>echo "dns-nameservers $from_user_fd $from_user_sd" >> $FN; break;;
<------>* ) echo "Please answer Yes or No.";;
    esac
done

echo "auto eth0" >> $FN

# ---------------------------------
# Try to config Nginx with FCGI
# ---------------------------------

apt-get install -y nginx python3 fcgiwrap

cat /dev/null > /etc/nginx/sites-enabled/default

read -p "Please write name of user for ngnix: " from_user_user;

FNG=/etc/nginx/sites-enabled/default

echo "server {" >> $FNG

echo "  listen 80;" >> $FNG
echo "  listen [::]:80;"  >> $FNG
echo "  charset utf-8;" >> $FNG
echo "  root /home/$from_user_user/www;" >> $FNG
echo "  index index.html index.htm; \n" >> $FNG
echo "  server_name localhost; \n" >> $FNG
echo "  location / {" >> $FNG
echo "      try_files $uri $uri/ =404;" >> $FNG
echo "  } \n" >> $FNG
echo "  location /cgi-bin/ {" >> $FNG
echo "      fastcgi_pass unix:/var/run/fcgiwrap.socket;" >> $FNG
echo "      include /etc/nginx/fastcgi_params;" >> $FNG
echo "      fastcgi_param SCRIPT_FILENAME /home/$from_user_user/www$fastcgi_script_name;" >> $FNG
echo "  }" >> $FNG
echo "}" >> $FNG

mkdir /home/$from_user_user/www

TP=/home/$from_user_user/www
cat /dev/null > $TP/test.py
echo "print('test')" >> $TP/test.py

chmod 755 $TP/test.py

cat /dev/null > $TP/index.html
echo "<h1>Hello World!</h1>" >> $TP/index.html

chmod 644 $TP/index.html

service nginx restart
service fcgiwrap restart

# ---------------------------------
# Install Postgress + create user and DB
# ---------------------------------

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main" > /etc/apt/sources.list.d/PostgreSQL.list'

apt update

apt-get -y install postgresql-10


read -p "-> Enter new user for DB: " from_user_name_user
read -sp "-> Enter password: " from_user_pass.
sudo -u postgres bash -c "psql -c \"create user $from_user_name_user with password '$from_user_pass';\""

read -p "-> Enter name of DB: " from_user_name_db
sudo -u postgres bash -c "psql -c \"create database $from_user_name_db;\""

sudo -u postgres bash -c "psql -c \"grant all privileges on database $from_user_name_db to $from_user_name_user;\""